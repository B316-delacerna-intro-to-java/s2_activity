package com.zuitt.activity;
import java.util.HashMap;

public class ActivityHashMaps {
    public static void main(String[] args) {
        HashMap<String, Integer> hygiene_kit = new HashMap<String, Integer>();

        hygiene_kit.put("toothpaste",15);
        hygiene_kit.put("toothbrush",20);
        hygiene_kit.put("soap",12);

        System.out.println("Our current inventory consists of: "+ hygiene_kit);
    }
}
