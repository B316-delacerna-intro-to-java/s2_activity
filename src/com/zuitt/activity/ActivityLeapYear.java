package com.zuitt.activity;

import java.util.Scanner;

public class ActivityLeapYear {
    public static void main(String[] args) {
        Scanner yearInputObj = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year.");

        String year = yearInputObj.nextLine();
        int convert_year = Integer.parseInt(year);

        if (convert_year % 4 == 0 && convert_year % 100 != 0 || convert_year % 400 == 0){
            System.out.println(convert_year + " is a leap year");
        } else {
            System.out.println(convert_year + " is NOT a leap year");
        }
    }
}
