package com.zuitt.activity;
import java.util.Scanner;

public class ActivityPrimeNumber {
    public static void main(String[] args) {
        int[] primeNumberArray = {2, 3, 5, 7, 11};

        Scanner inputIndexObj = new Scanner(System.in);
        System.out.println("Input array index between 0 to 4");

        String indexInput = inputIndexObj.nextLine();
        int convert_indexInput = Integer.parseInt(indexInput);

        switch (convert_indexInput){
            case 0:
                System.out.println("The first prime number is: " + primeNumberArray[convert_indexInput]);
                break;
            case 1:
                System.out.println("The second prime number is: " + primeNumberArray[convert_indexInput]);
                break;
            case 2:
                System.out.println("The third prime number is: " + primeNumberArray[convert_indexInput]);
                break;
            case 3:
                System.out.println("The fourth prime number is: " + primeNumberArray[convert_indexInput]);
                break;
            case 4:
                System.out.println("The fifth prime number is: " + primeNumberArray[convert_indexInput]);
                break;
            default:
                System.out.println("Invalid input");
        }

    }
}
