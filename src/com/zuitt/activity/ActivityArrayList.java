package com.zuitt.activity;
import java.util.ArrayList;

public class ActivityArrayList {
    public static void main(String[] args) {
        ArrayList<String> generics = new ArrayList<String>();
        //Adding elements to the ArrayList
        generics.add("John");
        generics.add("Jane");
        generics.add("Chloe");
        generics.add("Zoey");

        System.out.println("My friends are: " + generics);

    }
}
